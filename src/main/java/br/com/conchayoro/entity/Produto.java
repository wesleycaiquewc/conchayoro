package br.com.conchayoro.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name="Produto", uniqueConstraints = @UniqueConstraint(columnNames = "nome"))
public class Produto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long id = null;
 
	@NotNull	
	@Size(min = 1, max = 20)
	@Column(name="nome")	
	private String  nome;
 
	@NotNull
	@Column
	private String unidade;
	
	@NotNull
	@Column
	@DecimalMin(value = "0.1", inclusive = true)
	@DecimalMax(value = "99.9", inclusive = true)
	private Double precoUnitario;
	
	public Long getId() {
		return id;
	}
 
	public void setId(Long id) {
		this.id = id;
	}
 
	public String getNome() {
		return nome;
	}
 
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	public Double getPrecoUnitario() {
		return precoUnitario;
	}

	public void setPrecoUnitario(Double precoUnitario) {
		this.precoUnitario = precoUnitario;
	}
	
	public String getFaixa() {
		if (precoUnitario <= 100){
			return "Faixa1";}
		else 
			return "Faixa2";	    
	}
	
	public String toString() {
		return getId()+"; "+getNome()+"; "+getPrecoUnitario()+"; "+getUnidade();
	}
	 
 
}
